#!/bin/csh
# Script to start the ioc controller/server with sequencer.
# Use the loop back address, so the epics server and client can running on the same machine
#

setenv EPICS_CA_ADDR_LIST "127.0.0.1 192.168.1.156:5064 10.0.4.117"
setenv EPICS_CA_SERVER_PORT "5064"
setenv EPICS_CA_REPEATER_PORT "5065"
setenv EPICS_CA_AUTO_BEACON_ADDR "127.0.0.1"

/Users/juntongliu/epics/epics-apps/IOC_Lakeshore240/bin/darwin-x86/ls240  /Users/juntongliu/epics/epics-apps/IOC_Lakeshore240/iocBoot/iocls240/st.cmd
