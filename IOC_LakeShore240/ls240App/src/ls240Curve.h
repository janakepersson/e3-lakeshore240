
#define NUM_SPORTS 1
#define BUFFLEN 256    // max length can be read is 255 bytes

#define PORT 5002     // TCP port for control plane 
#define SA struct sockaddr

fd_set select_fds;
//char rcv_buff[BUFFLEN];

struct curve_header {
    char sensorModel[15];                  // This actually is the curve name in the Lakeshore 240 manual, Max length = 15
    char serialNumber[10];                 // serial number,
    unsigned short dataFormat;             // data format: 2 = V/K, 3 = Ω/K, 4 = log Ω/K
    double setPointLimit;                  // set point limit. It is curve temperature limimt, value in kelvin
    unsigned short temperatureCoefficient; // coefficient: 1 = negative, 2 = positive
    unsigned short numberBreakpoints;      // number of break points of the curve, max 200 the Model-240 can take
    char tempUnit;                         // K = kelvin, C = celsius, F = fahrenheit 
    unsigned short curveFileFormat;        // 1 = .340 format curve, 2 = test-curve
    //unsigned short input;                // The input module the curve will be uploaded to, 1 - 8
};

//char *serial_port = {"/dev/tty.SLAB_USBtoUART"};
struct curve_header curveHeader;
ELLLIST breakPointList;
typedef struct  breakPoint
{
    ELLNODE node;
    double sensor_unit;
    double temperature;
} breakPoint;

/* Variables for keeping track channels input type (for the 3 on/off buttons on the OPI) */
typedef struct intype_tracker 
{
    unsigned int channel_1;
    unsigned int channel_2;
    unsigned int channel_3;
    unsigned int channel_4;
    unsigned int channel_5;
    unsigned int channel_6;
    unsigned int channel_7;
    unsigned int channel_8;
}intype_tracker;

struct intype_tracker intypeTracker;


#define MAXNUMCURVEFILE  20
// struct curveFiles {
//     string curveFile1;
//     string curveFile2;
//     string curveFile3;
// };
struct onoff 
{
    int channelOnOff;
    int autorange;
    int currentReverse; 
} onoff;

enum {
    CRV_SEQ_OUT_RANGE = 1,    // curve file sequrnece number out of range, 1 - 20 for now
    NO_CRV_FILE,              // No such curve file
    BAD_FILENAME,             // Bad curve file name
    CRV_NO_ACCESS,            // Curve file is not accessable
    CRV_PARSE_ERROR,          // Curve file parse error
    CRV_BRP_ERROR,            // Curve file internal error
    CRV_EMPTY,                // EMPTY file
    CRV_FEW_BRP,              // too few breakpoints
    CRV_UPLOAD_ERROR
} ; //CRV_OPERA_ERROR;

//char *curveFiles[MAXNUMCURVEFILE] = {NULL};   // <<<<+================= Need change to standard later !

// enum {
//     MODEL=0, SERIAL, FORMAT, LIMIT, COEFFICIENT, BREAKPOINTS, UNIT
// }HEAD;

// define the max number of curve files that can be passed to OPI 

// Define header flag 
#define MODEL        0x01
#define NUMBER       0x02
#define FORMAT       0x04
#define LIMIT        0x08
#define COEFFICIENT  0x10
#define BREAKPOINTS  0x20
#define UNIT         0x40

#define DEBUG
#undef DEBUG
#ifdef DEBUG
#define dprintf(format, args...) printf(format, ##args)
#define DBGPRINT(fmt, ...)                            \
    do                                                \
    {                                                 \
        fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, \
                __LINE__, __func__, __VA_ARGS__);     \
    } while (0)
#else
#define dprintf(format, args...)
#define DBGPRINT(fmt, ...)
#endif