
#
#  Juntong Liu
#  @ European Spallation Source (ESS)
#  Email: juntong.liu@ess.eu
#								2020.07.16
#=============================================
#
#  This IOC consist of two planes: 1.) monitor plane, 2.) control plane. These two planes separate the data flow and control flow.
#  This is the stream protocol file for Lakeshore Model 240 temperature sensor transmitter.
#  
#  This stream protocol file is part of the monitor plane. 
#  
#
# File name: ls240.proto
#
#


Terminator="\r\n";
ReplyTimeout=1000;
ExtraInput = Ignore;

#####//// ------------------------ get/read protocols  --------------------------------

##### Get device ID information, and use the SCAN "I/O Intr" to get the sub fields. used for INIT records
getID{
	ExtraInput = Ignore;
	out "*IDN?";
	wait 10;
	in "%s";
	out "";
}
## Get device model number
getMODEL{
	in "LSCI,%11c,%*7c,%*s";
}
## Get device serial number
getSERIAL{
	in "LSCI,%*11c,%7c,%*s";
}
## Get Firmware version
getFIRMWARE{
	in "LSCI,%*11c,%*7c,%s";
}

#####
##### Get device ID Manufacture information
getIdManu{
	out "*IDN?";
	wait 10;
	in "%4c,%*11c,%*7c,%*5c";
	out "";
}
## Get module information
getIdModel{
	out "*IDN?";
	wait 10;
	in "%*4c,%11c,%*7c,%*5c";
	out "";
}
## Get Serial number
getIdSerial{
	out "*IDN?";
	wait 10;
	in "%*4c,%*11c,%7c,%*5c";
	out "";
}
## Get firm version
getIdFirm{
	out "*IDN?";
	wait 10;
	in "%*4c,%*11c,%*7c,%5c";
	out "";
}

#######
## get KRDG SRDG RDGST
getKRDGSRDG_R{
	out "KRDG? \$1;SRDG? \$1;RDGST? \$1";
	wait 15;
	in "%(A)9c%(B)9c%(C)c";
	out "";
}

## get KRDG SRDG
getKRDGSRDG{
	out "KRDG? \$1;SRDG? \$1";
	wait 15;
	#in "%9c%9c";
	in "%#s";
	out "";
}

## Get the related
getRELATED{
	out "KRDG? \$1;SRDG? \$1;RDGST? \$1;INNAME? \$1";
	wait 15;
	in "%#s";
	out "";
}
# getRELATED{
# 	out "KRDG? \$1;SRDG? \$1;RDGST? \$1;INNAME? \$1";
# 	wait 15;
# 	in "%8c;%8c;%c;%8c";
# 	out "";
# }


## Get device PROFIBUS address
getADDR{
	ExtraInput = Ignore;	
	out "ADDR?";
	wait 10;
	in "%39c";
	out "";
}

## Read tempterature from each channel in Kelvin
getKRDG{
	out "KRDG? \$1";
	wait 10;
	in "%f";
	out "";
}

## Get curve header
getCRVHDR{
	out "CRVHDR? \$1";
	wait 10;
	in "%#s";
	out "";
}

## Get curve name, 41
getCrvName{
	out "CRVHDR? \$1";
	wait 10;
	in "%15c,%*10c,%*c,%*10c,%*c";
	out "";
}

## Get curve serial  41 <<< ------------
getCrvSerial{
	out "CRVHDR? \$1";
	wait 10;
	in "%*15c,%10c,%*c,%*10c,%*c";
	out "";
}

## Get input type parameter
getINTYPE{
	out "INTYPE? \$1";
	wait 10;
	in "%s";
	out "";
}

## Get sensor type
getSensorType{
	ExtraInput = Ignore;
	out "INTYPE? \$1";
	wait 10;
	in "%c,%*c,%*c,%*c,%*c,%*c";
	out "";
}

## Get auto-range
getAutoRange{
	out "INTYPE? \$1";
	wait 10;
	in "%*c,%c,%*c,%*c,%*c,%*c";
	out "";
}

## Get range info
getRange{
	out "INTYPE? \$1";
	wait 10;
	in "%*c,%*c,%c,%*c,%*c,%*c";
	out "";
}

## Get current reverse info
getCurrentRev{
	out "INTYPE? \$1";
	wait 10;
	in "%*c,%*c,%*c,%c,%*c,%*c";
	out "";
}

## Get temperature unit info
getUnit{
	out "INTYPE? \$1";
	wait 10;
	in "%*c,%*c,%*c,%*c,%c,%*c";
	out "";
}

## Get flag: enable/disable
getFlag{
	out "INTYPE? \$1";
	wait 10;
	in "%*c,%*c,%*c,%*c,%*c,%c";
	out "";
}

## Get curve data point
getCRVPT{
	out "CRVPT? \$1 \$2";
	wait 10;
	in  "%s";
	out "";
}

getCRVPTunit{
	in  "CRVPT %f %*f";
}

getCRVPTtemp{
	in "CRVPT %*f %f";
}

getCRVPTunit0{
	out "CRVPT? \$1 \$2";
	wait 10;
	in  "CRVPT %1$f";
	out "";
}

getCRVPTtemp0{
	in "CRVPT %2$f";
}

## Get input filter paramter
getFILTER{
	out "FILTER? \$1";
	wait 10
	in "%f";
	out "";
}

## Read temperature from each channel in Fahrenheit
getFRDG{
	out "FRDG? \$1";
	wait 10;
	in "%f";
	out "";
}

## Get sensor input name
getINNAME{
	out "INNAME? \$1";
	wait 10;
	in "%#s";
	out "";
}

## Get/query the device module name
getMODNAME0{
	out "MODNAME?";
	wait 10;
	in "%s%s%s%s%*s";
	out "";
}

getMODNAME{
	ExtraInput = Ignore;
	out "MODNAME?";
	wait 10;
	in "%#s";
	out "";
}  

## Get PROFIBUS slot count
getPROFINUM{
	out "PROFINUM?";
	wait 10;
	in "%d";
	out "";
}

## Get/Read PROFIBUS slot configuration
getPROFISLOT{
	out "PROFISLOT? \$1";
	wait 10;
	in "%s";
	out "";
}

## Get/query PROFIBUS connection status
getPROFISTAT{
	out "PROFISTAT?";
	wait 10;
	int "%d";
	out "";
}

## Get input reading status
getRDGST{
	out "RDGST? \$1";
	wait 10;
	in "%s";
	out "";
}

## Get sensor units input reading
getSRDG{
	out "SRDG? \$1";
	wait 10;
	in "%f";
	#in "%#s";
	out "";      # this cause warnings when run IPSerialBridge.c on Raspberry, and IOC on MAC OSX: input "" does not match "%f" 
}

## Get front panel brightness
getBRIGT{
	out "BRIGT?";
	wait 10;
	in "%s";
	out "";
}

########## set protocols

## Set front console brightness
setBRIGT{
    out "BRIGT \$1";
    wait 15;
    out "BRIGT?";
    out "";
}

## Set/configure the PROFBUS address
#setADDR{
#	out "ADDR \$1";
#	@init {
#		getADDR;
#	}
#}
setADDR{
	out "ADDR \$1";
	wait 15;
	out "ADDR?";
	out "";
}

## Delete user curve for an input
setCRVDEL{
	out "CRVDEL \$1";
	wait 15;
	out "";
}

## set/configure the user curve header(check to se if this set the header in EPROM or memory)
setCRVHDR{
	out "CRVHDR \$1,\$2,\$3,\$4,\$5,\$6";
	wait 20;
	out "CRVHDR? \$1";
	out "";
}

# set/configure user curve data point
setCRVPT{
	out "CRVPT \$1,\$2,\$3,\$4,\$5";
	wait 20;
	out "CRVPT? \$1,\$2";
	out "";
}

# Set/restore factory default settings
setDFLT{
	out "DFLT \$1";     ## we keep the number secrete, so do not let someone resore the instrument accidentally
	wait 15;
	out "";
}

# set filter only valid for model 240-2P

# set input name, max length: 15 chars
setINNAME{
	out "INNAME \$1,\$2";
	wait 15;
	out "INNAME? \$1";
	out "";
}

# Set/Confiure input type parameter
setINTYPE{
	out "INTYPE \$1,\$2,\$3,\$4,\$5,\$6,\$7";
	wait 20;
	out "INTYPE? \$1";
	out "";
}

# set/configure mudule name(max. 32 chars)
setMODNAME{
	out "MODNAME \$1";
	wait 15;
	out "MODNAME?"
	out "";
}

# set/configure PROFIBUS slot count
setPROFINUM{
	out "PROFINUM \$1";
	wait 15;
	out "PROFINUM?";
	out "";
}

# set/configure PROFISLOT
setPROFISLOT{
	out "PROFISLOT \$1,\$2,\$3";
	wait 15;
	out "PROFISLOT? \$2";
	out "";
}

## Debug, to get a generic command interface.
## After processing finished, the record contains the reply.
debugMSG{
	ExtraInput = Ignore;
	out "%s";
	wait 10;
	in "%39c";
}

